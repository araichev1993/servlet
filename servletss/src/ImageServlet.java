import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class ImageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        String contextPath = servletContext.getRealPath(File.separator);
        File file = new File(contextPath + "\\picture.jpg");
        byte[] arr = new byte[(int) file.length()];

        FileInputStream fileInput = new FileInputStream(file);
        fileInput.read(arr);
        fileInput.close();


        ByteArrayInputStream iStream = new ByteArrayInputStream(arr);

        int length = arr.length;

        response.setContentType("image/jpeg");
        response.setContentLength(length);

        ServletOutputStream oStream = response.getOutputStream();

        byte[] buffer = new byte[1024];
        int len;
        while ((len = iStream.read(buffer)) != -1) {
            oStream.write(buffer, 0, len);
        }

        iStream.close();

        oStream.flush();
        oStream.close();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
