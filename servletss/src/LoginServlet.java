import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Aleksandar on 11/24/2016.
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = "svilen";
        String password = "svilen123";

        String nameRequest = request.getParameter("name");
        String passRequest = request.getParameter("pass");

        if (nameRequest.equals(name) && passRequest.equals(password)) {
            RequestDispatcher rd = request.getRequestDispatcher("/showPicture");
            rd.forward(request, response);

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("index.html");
            rd.include(request, response);

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
